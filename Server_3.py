from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import socket
import termcolor2

key = RSA.generate(2048)
pubKey =  key.publickey()
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 8000
host = '127.0.0.1'

try:
    s.bind((host, port))
    s.listen()
    print(termcolor2.colored("Server Successfully Started", color='green'))
except:
    print(termcolor2.colored("Server Error!", color='red'))

c, addr = s.accept()

while True:

    # Decryption
    cipher = PKCS1_OAEP.new(key)
    print("Received Message :", s.recv(1024))
    print('Decrypted:', cipher.decrypt(cipherText))

    message = input(termcolor2.colored('Your Message : ', color='blue'))

    if message == "":
        print(termcolor2.colored("Error:Dont send Empty body message!", color='red'))
        message = input(termcolor2.colored('Your Message : ', color='blue'))

    if message != "GoodBye":
        # Encryption
        cipher = PKCS1_OAEP.new(pubKey)
        cipherText = cipher.encrypt(message.encode())
        print("Encrypted Message: ", cipherText)
        s.send(cipherText)

    else:
        print(termcolor2.colored("Connection Closed from Server Side", color='green'))
        c.close()
        exit(0)
