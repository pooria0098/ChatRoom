from .Encription import *
import socket
import termcolor2

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 8000
host = '127.0.0.1'

try:
    s.connect((host, port))
    print(termcolor2.colored("Successfully Client Connected to Server",color='green'))
except ConnectionRefusedError:
    print(termcolor2.colored("Refused Connect to Server",color='red'))
    exit(0)

while True:
    message = input(termcolor2.colored('Your Message :',color='blue'))
    if message == "":
        print(termcolor2.colored("Error:Dont send Empty body message!",color='red'))
        continue

    if message != "GoodBye":
        # Encryption
        cipher = PKCS1_OAEP.new(pubKey)
        cipherText = cipher.encrypt(message.encode())
        print("Encrypted Message: ",cipherText)
        s.send(cipherText)

        # Decryption
        cipher = PKCS1_OAEP.new(key)
        print("Received Message :", s.recv(1024))
        print('Decrypted:', cipher.decrypt(cipherText))

    else:
        s.close()
        print(termcolor2.colored("Connection Closed from Client Side",color='green'))
        exit(0)

